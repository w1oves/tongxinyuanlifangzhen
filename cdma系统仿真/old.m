%无相干解调 
clear;clc;close all;
%% 参数输入部分
image_flag=input('输入1：传输图片；0：传输二进制序列:  '); % 1：传输对象为图片；0：传输对象为简单二进制序列
if image_flag==0
    rand_flag=input('输入1:传输随机长度序列,输入0:传输三个码元长度的二进制序列:');
else
    rand_flag=0;
end
snr_image=input('输入需要显示的图片的信噪比,单位（dB）: '); % 接收图片信噪比，单位（dB），范围：-50~10 dB （10的整数倍）
if image_flag==1
    BER_flag=input('输入1：计算误比特率；输入0：不计算误比特率：  ');   % 1：计算误比特率曲线；0：不计算误比特率曲线
    Rs=input('输入符号传输速率,范围：1000~~20000，单位：bps：   ');      % 符号传输速率,范围：0~20000，单位：bps
else
    BER_flag=0;   % 1：计算误比特率曲线；0：不计算误比特率曲线
    Rs=1000;
end
%% 输入参数验证部分
image_flag_vertify=[1 0];
rand_flag_vertify=[1 0];
snr_image_vertify=[-50 10];
BER_flag_vertify=[1 0];
Rs_vertify=[1000 5000 20000];
if ~any(image_flag_vertify==image_flag)
    image_flag=image_flag_vertify(1);
    disp('警告：存在输入错误，设置为默认值');
end
if ~any(rand_flag_vertify==rand_flag)
    rand_flag=image_flag_vertify(1);
    disp('警告：存在输入错误，设置为默认值');
end
if snr_image_vertify(1)>snr_image || snr_image_vertify(end)<snr_image
    snr_image=snr_image_vertify(1);
    disp('警告：存在输入错误，设置为默认值');
end
if ~any(BER_flag_vertify==BER_flag)
    BER_flag=BER_flag_vertify(1);
    disp('警告：存在输入错误，设置为默认值');
end
if Rs_vertify(1)>Rs || Rs_vertify(end)<Rs
    Rs=Rs_vertify(2);
    disp('警告：存在输入错误，设置为默认值');
end
%% 准备工作，构造待测试信噪比序列
SNR=[];
flag=1;
for snr=-50:5:10
    if flag
        if snr>=snr_image
            if snr>snr_image
                SNR=[SNR snr_image];
            end
            flag=0;
        end
    end
    SNR=[SNR snr];    
end
BER=[];
if BER_flag==0
    SNR=snr_image;
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%发送端%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 读取输入图片，构建矩阵input，其大小为：图片数量-像素数量-RGB
image1=imread('1.bmp');
image2=imread('2.bmp');
image3=imread('3.bmp');
image4=imread('4.bmp');
w=120;h=120;
input_image=[reshape(image1,1,w*h,3);reshape(image2,1,w*h,3);reshape(image3,1,w*h,3);reshape(image4,1,w*h,3)];
if image_flag==1
    paint(input_image,1,'input','原始图片');
end
disp('图片读取完毕...');

%% 量化得到待传输数据data，其大小为：图片数量-像素数量
input_data=reshape(sum(permute(input_image,[3 1 2]))>255*3/2,size(input_image,1),size(input_image,2));
if image_flag==0
    if rand_flag==0
        input_data=[1 0 1;0 1 0; 0 0 0; 1 1 1];
    else
        input_data=randi([0 1],4,256);
    end
    figure(1);
    subplot(6,1,1);
    [x,y]=getSignal(input_data(1,:));
    plot(15*x,y);
    title('原始信号');ylabel('A');xlabel('t/ms');
    axis([0,(length(x)+1)*15/100,-0.5,1.5]);
end
data_N=size(input_data,1);
data_L=size(input_data,2);
disp('量化完毕...');

%% 获取gold码矩阵gold，其大小为：用户数量-码长
gold=Gold();
gold_L=size(gold,1);
disp('成功生成gold码...');

%% 扩频
% 构建发送信号矩阵sendCode，其大小为：用户数量-（数据长度*码长）
spreadCode=zeros(data_N,data_L*gold_L);
for i=1:data_N
    left=1;
    right=gold_L;
    for j=1:data_L
        spreadCode(i,left:right)=mod(gold(:,i)'+input_data(i,j),2);%模2和
        left=left+gold_L;
        right=right+gold_L;
    end
end
if image_flag==0&&rand_flag==1
    plotfft(input_data(1,:),spreadCode(1,:));
end
%% 单极性码变换为双极性码
spreadCode=2*spreadCode-1;
disp('完成扩频...');

%% 调制
fc=Rs;%载波频率
fs=44100;%抽样频率
fs=fs+Rs-mod(fs,Rs);
wc=2*pi*fc;%载波角频率
N=length(spreadCode);
t=0:1/fs:N/Rs-1/fs;%时域
n=length(t);
signal=zeros(data_N,n);%构建信号
for count=1:data_N
    signal(count,:)=repelem(spreadCode(count,:),n/N);
end
modulation=signal(:,:).*sin(wc*t);%调制信号
if image_flag==0%画图
    figure(1);
    subplot(6,1,2);
    plot(1000*t,signal(1,:));
    title('扩频信号'); ylabel('A');xlabel('t/ms');
    axis([0,N/Rs*1000,-1.5,1.5]);
    subplot(6,1,3);

    plot(1000*t,modulation(1,:));
    title('调制信号');ylabel('A');xlabel('t/ms');
    axis([0,N/Rs*1000,-1.5,1.5]);
end
disp('完成调制...');

%% awgn信道加噪声
if BER_flag==1&&image_flag==1
    disp('开始计算误比特率...');
end
for snr=SNR
    if BER_flag==1
        recSignal=awgn(modulation,snr);
    else
        recSignal=awgn(modulation,snr_image);
    end
    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%接收端%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %% 解调
    demodulation=recSignal(:,:).*sin(wc*t);
    recCode=zeros(size(spreadCode));
    for count=1:data_N
        recCode(count,:)=sum(reshape(demodulation(count,:),n/N,N))>0;
    end
    if image_flag==0
        figure(1);
        subplot(6,1,4);
        plot(1000*t,demodulation(1,:));
        title('相干解调中间信号');ylabel('A');xlabel('t/ms');
        axis([0,N/Rs*1000,-1.5,1.5]);
        subplot(6,1,5);
        
        [x,y]=getSignal(recCode(1,:));
        plot(1000*x,y);
        title('解调信号');ylabel('A');xlabel('t/ms');
        axis([0,(length(x)+1)*10,-1.5,1.5]);
    end
    if BER_flag==0
        disp('成功解调...');
    end
    %% 解扩
    despreadCode=zeros(size(recCode));
    for i=1:size(recCode,1)
        left=1;
        right=gold_L;
        while right<=size(recCode,2)
            despreadCode(i,left:right)=mod(recCode(i,left:right)+(gold(:,i)'),2);
            left=left+gold_L;
            right=right+gold_L;
        end
    end
    recData=zeros(data_N,data_L);
    for count=1:data_N
        recData(count,:)=sum(reshape(despreadCode(count,:),gold_L,data_L))>gold_L/2;
    end
    if image_flag==0
        figure(1);
        subplot(6,1,6);
        [x,y]=getSignal(recData(1,:));
        plot(15*x,y);
        title('解扩信号');
        ylabel('A');
        xlabel('t/ms');
        axis([0,(length(x)+1)*15/100,-0.5,1.5]);
    end
    if BER_flag==0
        disp('成功解扩...');
    end
    ber=mean(mean(input_data~=recData))*100;
    disp(['SNR:',num2str(snr),'db    BER:',num2str(ber),'%']);
    BER=[BER ber];
    if image_flag==1
        %图片解压
        output=repmat(recData*255,1,3);
        if BER_flag==0
            disp('图片解压并输出');
        end
        if snr==snr_image||BER_flag==0
            paint(output,2,'output',['信噪比：',num2str(snr),' dB']);
        end
    end
end
%% 画误比特率曲线图
if length(SNR)>1
    gcf=figure(3);
    NRZ=[];
    BNRZ=[];
    for snr=SNR
        temp=10^(snr/10);
        NRZ=[NRZ 100*qfunc(sqrt(temp/2))];
        BNRZ=[BNRZ 100*qfunc(sqrt(temp))];
    end
    set(gcf,'Name','BER');
    set(gcf,'units','normalized','position',[0.3 0.1 0.3 0.3]);
    plot(SNR,BER,SNR,NRZ,SNR,BNRZ);
    grid on;
    xlabel('SNR(dB)');ylabel('误码率(%)');
    legend('CDMA仿真误码率曲线','NRZ误码率曲线','BNRZ仿真误码率曲线');
end