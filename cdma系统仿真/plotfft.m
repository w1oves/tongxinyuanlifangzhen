function [] = plotfft(data,spread)
%PLOTFFT 此处显示有关此函数的摘要%   此处显示详细说明
figure(5);
fs=256;
s1=repelem(data,15*fs);
s2=repelem(spread,fs);
f1=fftshift(fft(s1));
f2=fftshift(fft(s2));
f1=f1.*conj(f1);
f1=10*log10(f1);
f2=f2.*conj(f2);
f2=10*log10(f2);
N=length(f1);
fN=N-mod(N,2);
k=-fN/2:1:fN/2-1;
f=k*fs/N;
zero_point=fN/2+2;
right_point=zero_point+ceil(5*fN/fs);
left_point=2*zero_point-right_point;
subplot(2,1,1);
plot(f(left_point:right_point),f1(left_point:right_point));
axis([f(left_point),f(right_point),0,max(f2(left_point:right_point))]);
ylabel('P(f)');
xlabel('归一化频率f/fs');
title('基带信号频谱');
subplot(2,1,2);
plot(f(left_point:right_point),f2(left_point:right_point));
axis([f(left_point),f(right_point),0,max(f2(left_point:right_point))]);
ylabel('P(f)');
xlabel('归一化频率f/fs');
title('扩频信号频谱');
end

