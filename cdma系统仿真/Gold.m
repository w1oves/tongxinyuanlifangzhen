function [output] = Gold()
%GOLD 产生大小为255*8的GOLD码矩阵
%   此处显示详细说明
clc;
clear;
a=mSequence([1 1 1 1]);
b=mSequence([1 0 1 0]);
output=mod(a+b,2);
end

function [output] = mSequence(initial)
PN=[1 0 0 1];
%% calculate
n=length(PN);
row=2^n;
state=zeros(row-1,n);
state(1,:)=initial;
count=2;
while count<row
    state(count,:)=circshift(state(count-1,:)',1)';
    state(count,1)=mod(state(count-1,:)*(PN'),2);
    count=count+1;
end
output=state;
end