function varargout = click_here(varargin)
%% GUI界面部分
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @codeGui_OpeningFcn, ...
                   'gui_OutputFcn',  @codeGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

function codeGui_OpeningFcn(hObject, ~, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
global flag;
flag=0;

function varargout = codeGui_OutputFcn(~, ~, handles) 
varargout{1} = handles.output;

function edit1_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function edit1_Callback(~, ~, ~)


function pushbutton1_Callback(~, ~, handles)
%% 获取输入并调用codeConvert函数
global flag data t fs rate NRZ BNRZ RZ BRZ NRZM NRZS MCST MILLER CMI AMI HDB3;
str=get(handles.edit1,'string');
data=eval(cell2mat({'[',str,']'}));
disp(data);
isValid=1;
if length(data)<1 || sum(data~=1 & data~=0)>0
    isValid=0;
end
if isValid==1
    set(handles.text4,'string','转换成功');
    [t,fs,rate,NRZ,BNRZ,RZ,BRZ,NRZM,NRZS,MCST,MILLER,CMI,AMI,HDB3]=codeConvert(data); 
    flag=1;
else 
    set(handles.text4,'string','输入不合法，请重新输入');
end

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
%% 获取用户选择项并画出对应波形、功率谱
global flag;
if flag==0
    set(handles.text7,'string','请先点击上方确认绘制');
    return;
else
    set(handles.text7,'string','选择以查看功率谱');
end
global data t fs rate NRZ BNRZ RZ BRZ NRZM NRZS MCST MILLER CMI AMI HDB3;
val=get(handles.popupmenu1,'value');
switch val
    case 1
        code=NRZ;
    case 2
        code=BNRZ;
    case 3
        code=RZ;
    case 4 
        code=BRZ;
    case 5
        code=NRZM;
    case 6
        code=NRZS;
    case 7
        code=MCST;
    case 8
        code=MILLER;
    case 9
        code=CMI;
    case 10
        code=AMI;
    case 11
        code=HDB3;
end
N=length(code);
F=fftshift(fft(code));%傅里叶变换
P=F.*conj(F)/N;%获取功率谱
P=10*log10(P);%转换为db单位
%设置频率轴
fN=N-mod(N,2);
k=-fN/2:1:fN/2-1;
f=k*fs/N/rate;%rate在这里实际上指的是基带信号速率
zero_point=fN/2+2;
right_point=zero_point+ceil(4*rate*fN/fs);
left_point=2*zero_point-right_point;
P=P-min(P(left_point:right_point));

%画图
grid on;
plot(handles.axes1,f(left_point:right_point),P(left_point:right_point));
plot(handles.axes4,t,code);
%===============初始化坐标轴设置================
set(handles.axes4,'XLim',[0,1000*length(data)/rate],'YLim',[1.5*(-5),1.5*5]);
set(handles.axes4,'XGrid','on','Ygrid','on');
set(handles.axes4,'XTick',0:1000*1/rate:1000*N/rate);
set(handles.axes4,'YTick',(-5):5:5);
set(handles.axes1,'XLim',[f(left_point),f(right_point)]);
set(get(handles.axes4,'title'),'string','时域波形');
set(get(handles.axes4,'ylabel'),'string','幅度/v');
set(get(handles.axes4,'xlabel'),'string','时间/ms');
set(get(handles.axes1,'title'),'string','功率谱波形');
set(get(handles.axes1,'ylabel'),'string','P(f)');
set(get(handles.axes1,'xlabel'),'string','归一化频率/fs');

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
