function [code_t,code_fs,code_rate,NRZ,BNRZ,RZ,BRZ,NRZM,NRZS,MCST,MILLER,CMI,AMI,HDB3] = codeConvert(signal)
%% 编码转换函数
%% 函数功能：将二进制向量signal转换为各类编码并且画出
%signal=[1 0 0 1 1 0 0 0 0 0 1 1 0 0 0 0 0 1 0 1];
%% 参数设置
global positive zero negative rate fs;
positive=5;%高电平电压值
zero=0;%零电平电压值
negative=-5;%低电平电压值
fs=65536;%抽样率:每秒采样点数
rate=100;%传输速率：每秒传输符号数
%% 显示设置：调整窗口大小与布局：2*6
global w_num h_num number;
gcf=figure;
set(gcf,'units','normalized','position',[0.05 0.6 0.25 0.3]);
set(gcf,'NumberTitle', 'off', 'Name', int2str(signal));
w_num=2;
h_num=3;
number=1;
%% 时间轴构造
global N n;
N=length(signal);
t=0:1/fs:N/rate;
code_t=t*1000;
n=length(t);
%% 单极性非归零码
NRZ=signalToCode(signal);
show(1000*t,NRZ,'单极性非归零码');

%% 双极性非归零码
bnrz=signal;
for count=1:1:N
    if bnrz(count)==0
        bnrz(count)=-1;
    end
end
BNRZ=signalToCode(bnrz);
show(1000*t,BNRZ,'双极性非归零码');
%% 单极性归零码
rz=linspace(0,0,2*N);
for count=1:1:N
    rz(count*2-1)=signal(count);
    rz(count*2)=0;
end
RZ=signalToCode(rz);
show(1000*t,RZ,'单极性归零码');
%% 双极性归零码
brz=linspace(0,0,2*N);
for count=1:1:N
    if signal(count)==1
        brz(count*2-1)=1;
    else
        brz(count*2-1)=-1;
    end
    brz(count*2)=0;
end
BRZ=signalToCode(brz);
show(1000*t,BRZ,'双极性归零码');
%% 传号差分码
nrzm=linspace(1,1,N);
for count=2:1:N
    nrzm(count)=mod(signal(count)+nrzm(count-1),2);
end
NRZM=signalToCode(nrzm);
show(1000*t,NRZM,'传号差分码');
%% 空号差分码
nrzs=linspace(0,0,N);
for count=2:1:N
    if signal(count)==0
        nrzs(count)=mod(nrzs(count-1)+1,2);
    else
        nrzs(count)=nrzs(count-1);
    end
end
NRZS=signalToCode(nrzs);
show(1000*t,NRZS,'空号差分码');

%% 第二张图片
gcf=figure;
set(gcf,'units','normalized','position',[0.3 0.6 0.25 0.3]);
set(gcf,'NumberTitle', 'off', 'Name', int2str(signal));
h_num=2;
number=1;

%% 数字双相码
mcst=linspace(0,0,2*N);
for count=1:1:N
    mcst(count*2-1)=mod(signal(count)+1,2)*2-1;
    mcst(count*2)=signal(count)*2-1;
end
MCST=signalToCode(mcst);
show(1000*t,MCST,'数字双相码');

%% 密勒码
miller=linspace(0,0,2*N);
miller(1)=-1;
for count=2:1:2*N
    if mcst(count)-mcst(count-1)==2
        miller(count)=miller(count-1)*(-1);
    else
        miller(count)=miller(count-1);
    end
end
MILLER=signalToCode(miller);

show(1000*t,MILLER,'密勒码');

%% 传号反转码
cmi=linspace(0,0,2*N);
state=1;
for count=1:1:N
    if signal(count)==1
        cmi(2*count-1)=state;
        cmi(2*count)=state;
        state=-state;
    else
        cmi(2*count-1)=-1;
        cmi(2*count)=1;
    end
end
CMI=signalToCode(cmi);
show(1000*t,CMI,'传号反转码');

%% AMI码
ami=linspace(0,0,2*N);
state=1;
for count=1:1:N
    if signal(count)==1
        ami(2*count-1)=state;
        ami(2*count)=0;
        state=-state;
    else
        ami(2*count-1)=0;
        ami(2*count)=0;
    end
end
AMI=signalToCode(ami);
show(1000*t,AMI,'AMI码');

%% 第三张图片
gcf=figure;
set(gcf,'units','normalized','position',[0.05 0.2 0.25 0.3]);
set(gcf,'NumberTitle', 'off', 'Name', int2str(signal));
h_num=1;
w_num=1;
number=1;
%% HDB3码
B=1;
V=1;
hdb3=linspace(0,0,2*N);
str=linspace(0,0,N);
zero_times=0;
for count=1:1:N
    if signal(count)==0
        zero_times=zero_times+1;
        if zero_times==4
            hdb3(2*count-1)=V;
            hdb3(2*count)=0;
            str(count)=-1;
            V=-V;
            if V~=B
                hdb3(2*(count-3)-1)=B;
                hdb3(2*(count-3))=0;
                str(count-3)=1;
                B=-B;
            end
            zero_times=0;
        else
            hdb3(2*count-1)=0;
            hdb3(2*count)=0;
        end
    else
        hdb3(2*count-1)=B;
        hdb3(2*count)=0;
        str(count)=1;
        B=-B;
        zero_times=0;
    end
end
HDB3=signalToCode(hdb3);
show(1000*t,HDB3,'HDB3码');
for count=1:1:N
    if str(count)==1
        if hdb3(2*count-1)==1
            text(1000*(count-1)/N*n/fs,positive+1,'B+','color','BLACK');
        else
            text(1000*(count-1)/N*n/fs,negative-1,'B-','color','BLACK');
        end
    elseif str(count)==-1
        if hdb3(2*count-1)==1
            text(1000*(count-1)/N*n/fs,positive+1,'V+','color','red');
        else
            text(1000*(count-1)/N*n/fs,negative-1,'V-','color','red');
        end
    end
end
%% 返回值处理
code_fs=fs;
code_rate=rate;
end
%% 基本的二进制数据编码函数（NRZ和BNRZ）
function [Code]=signalToCode(signal)
global rate fs N n zero positive negative;
N2=length(signal);
k=fix(N2/N);
Code=linspace(0,0,n);
for count=1:1:n
    current=min(ceil(k*count*rate/fs),k*N);
    if signal(current)==1
        Code(count)=positive;
    elseif signal(current)==-1
        Code(count)=negative;
    else
        Code(count)=zero;
    end
end
end
%% 绘图函数
function [] = show(t,y,str)
global positive negative w_num h_num number N rate;
subplot(h_num,w_num,number);
number=number+1;
plot(t,y);
grid on;
title(str);
axis([0,1000*N/rate,1.5*negative,1.5*positive]);
set(gca,'XTick',0:1000*1/rate:1000*N/rate);
set(gca,'YTick',negative:positive:positive);
xlabel('time/ms');
ylabel('volt/v');
end

