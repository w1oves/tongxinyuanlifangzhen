# 通信原理仿真

#### 介绍
通信原理相关仿真，包括常用数字基带信号码型仿真和cdma系统仿真(注意：需要四张120*120像素的图像）
cdma系统中，simulation.m为无相干解调的版本，simulation2.m为有相干解调的版本

![cdma系统仿真流程图](https://images.gitee.com/uploads/images/2020/0616/202520_cc7a9478_6580628.png "未命名文件 (3).png")